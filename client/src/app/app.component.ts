import { Component } from '@angular/core';
import { DataService } from './service/data.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'client';
  employeeData;
  showForm = false;
  editEmployeeData: any;
  constructor(private dataService: DataService) {
    dataService.getEmployee(data => {
      console.log(data);
      this.employeeData = data;
    });
  }

  insertEmployee(firstName, lastName){
    const dataForSend = {firstName, lastName};
    this.dataService.insertEmployee(dataForSend, (resp) => {
      console.log('Response from server: ', resp);
      if(resp) {
        this.employeeData.push(dataForSend)
      }
    })
  }

  deleteEmployee(_id){
    const dataToDelete = {_id};
    this.dataService.deleteEmployee(dataToDelete, res=>{
      this.employeeData = this.employeeData.filter(emp=>{
        return  emp._id !== _id
      });
    })
  }

  updateEmployee(){
    this.dataService.updateEmployee({emp: this.editEmployeeData.emp}, (res) => {
      this.employeeData[this.editEmployeeData.ind] = this.editEmployeeData.emp;
      if(res) {
        this.showForm = false;
      }
    });
  }
  openForm(emp, ind) {
    this.showForm = true;
    this.editEmployeeData = {emp, ind};
  }

  closeForm() {
    this.showForm = false;
  }
}
