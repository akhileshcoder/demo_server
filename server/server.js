var express       = require('express');
var bodyParser    = require('body-parser');
var path    = require('path');
var cors    = require('cors');
var {ObjectId}    = require('mongodb');
var {getData, insertData, updateData, deleteData} = require('./mongo');
var app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
const staticPath = path.resolve(__dirname, '../static');
app.use(express.static(staticPath));

app.post('/get_table_data', function (req, res) {
    const tableName = req.body.type;
    getData({}, tableName, (data)=>{
        res.send(data);
    })
});

app.get('/get_employee', (req, res)=>{
    getData({}, 'employee', data=>{
        res.send(data);
    });
});
app.put('/insert_employee', (req, res)=> {
    console.log('req.body: ', req.body);
    insertData([req.body], 'employee', (err, data) => {
        console.log(err, data);
        res.send(data);
    });
});
app.post('/update_employee', (req, res)=>{
        console.log('req.body: ', req.body);
        const {_id, firstName, lastName} = req.body.emp;
        updateData({_id: ObjectId(_id)}, {firstName, lastName}, 'employee', (err, data)=>{
            //console.log(err, data);
            res.send(data);
        });
});

app.post('/delete_employee', (req, res)=> {
    console.log('req.body: ', req.body);
    deleteData({_id: ObjectId(req.body._id)}, 'employee', data => {
        res.send(data);
    })
});
    const port = 5001;
    app.listen(port, function () {
        console.log("server stared at : ", port);
    });
